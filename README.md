# Freshman To Fresh Grad
Provides information to incoming college students about schools of interest and post-graduation opportunities
### Website
The project website is freshman2freshgrad.com
### Presentation link
https://www.youtube.com/watch?v=IOgISoaAYok
## Group Members
| Name  | EID | GitLab Username |
| ------------- | ------------- | ------------- |
| Amir Mostafavi | am85334 | amirmst |
| Sooyong Lee | sl46538 | sooyonglee |
| Chandler Ochs | co7882 | chandlerochs |
| Zachary May | zsm334 | ZacharyMay |
| Dillan Holand | djh3625  | dillan0321 |
| Connor Hubbard | ch44389  | hubchub |
## Git SHA
| Phase | Git SHA |
| ------------- | ------------- |
| Phase I | b9c2ca137fffd6dcda7dcb6d66685f9792f167eb |
| Phase II | b542dde644844bef5e6da8cd345b858b104bbf49 |
| Phase III | 023d74f22dddabbee9ef8b4bc82a87937ab68b4d |
| Phase IV | --------------------------------------- |
## GitLab Pipelines
The GitLab pipelies of the project can be found [here](https://gitlab.com/amirmst/freshman-2-freshgrad/pipelines).
## Completion Times (per member)
| Member | Phase | Estimated Completion Time | Actual Completion Time |
| ------------- | ------------- | ------------- | ------------- |
| Amir | IV | 10 hours | 15 hours |
| Zac | IV | 5 hours | 15 hours |
| Dillan | IV | 5 hours | 10 hours |
| Sooyong | IV | 20 hours | 35 hours |
| Chandler | IV | 5 hours | 15 hours |
| Connor | IV | ---- | ---- |
| Amir | III | 10 hours | 10 hours |
| Zac | III | 15 hours | 10 hours |
| Dillan | III | 5 hours | 5 hours |
| Sooyong | III | 20 hours | 20 hours |
| Chandler | III | 10 hours | 10 hours |
| Connor | III | ---- | ---- |
| Amir | II | 30 hours | 45 hours |
| Zac | II | 40 hours | 75 hours |
| Dillan | II | 25 hours | 35 hours |
| Sooyong | II | 30 hours | 50 hours |
| Chandler | II | 30 hours | 40 hours |
| Connor | II | ---- | 0 |
| Amir | I | 10 hours | 20 hours |
| Zac | I | 15 hours | 20 hours |
| Dillan | I | 15 hours | 20 hours |
| Sooyong | I | 10 hours | 25 hours |
| Chandler | I | 15 hours | 20 hours |
| Connor | I | ---- | ---- |

## Notes
