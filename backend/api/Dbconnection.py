import urllib
import sqlalchemy as sa
from sqlalchemy import join
from sqlalchemy import and_, or_, not_
from sqlalchemy.sql import text
from re import sub
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from ApiModels import City, University, Company, STATE_NAMES_TO_CODES

Base = declarative_base()

host = "f2fgdevlt.cu7dlxcba8he.us-east-2.rds.amazonaws.com"
port = 1431
dbname = "F2FG"
user = "admin"
password = "downing373t610am!"


params = urllib.parse.quote_plus(
    "DRIVER={ODBC Driver 17 for SQL Server};"
    "SERVER=f2fgdevlt.cu7dlxcba8he.us-east-2.rds.amazonaws.com,1431;"
    "DATABASE=F2FG;"
    "UID=admin;"
    "PWD=downing373t610am!"
)


engine = sa.create_engine("mssql+pyodbc:///?odbc_connect={}".format(params))

# engine = sa.create_engine("mssql+pymssql://admin:downing373t610am!@f2fgdevlt.cu7dlxcba8he.us-east-2.rds.amazonaws.com:1431/F2FG")

"""
Cities Functions
"""


def get_all_cities():
    Session = sessionmaker(bind=engine)
    session = Session()
    cities = []
    for instance in session.query(City).all():
        city_data = {
            "city": instance.City,
            "state": instance.State,
            "quality_of_life_index": instance.QualityOfLifeIndex,
            "rent_index": instance.RentIndex,
            "crime_index": instance.CrimeIndex,
            "image_url": instance.ImageUrl,
            "population": instance.Population,
            "area": 123,
            "traffic_time_index": instance.TrafficTimeIndex,
        }
        cities.append(city_data)
    return cities


def get_cities_by_state(*, state):
    Session = sessionmaker(bind=engine)
    session = Session()
    state_code = get_state_code(state)
    result = session.query(City).filter(City.State == state_code)
    cities = []
    for instance in result:
        city_data = {
            "city": instance.City,
            "state": instance.State,
            "quality_of_life_index": instance.QualityOfLifeIndex,
            "rent_index": instance.RentIndex,
            "crime_index": instance.CrimeIndex,
            "image_url": instance.ImageUrl,
            "population": instance.Population,
            "area": 123,
            "traffic_time_index": instance.TrafficTimeIndex,
        }
        cities.append(city_data)
    return cities if len(cities) > 0 else None


def get_city(*, city, state):
    Session = sessionmaker(bind=engine)
    session = Session()
    state_code = get_state_code(state)
    city = sub("[-,\s]+", "%", city)
    result = session.query(City).filter(
        City.City.ilike("%" + city + "%"), City.State == state_code
    )
    it = iter(result)
    result = None
    try:
        instance = next(it)
    except StopIteration:
        return result

    city_data = {
        "city": instance.City,
        "state": instance.State,
        "latitude": instance.Latitude,
        "longitude": instance.Longitude,
        "crime_index": instance.CrimeIndex,
        "health_care_index": instance.HealthCareIndex,
        "traffic_time_index": instance.TrafficTimeIndex,
        "quality_of_life_index": instance.QualityOfLifeIndex,
        "cpi_index": instance.CPIIndex,
        "rent_index": instance.RentIndex,
        "population": instance.Population,
        "image_url": instance.ImageUrl,
    }
    result = city_data
    return result


# TODO: Fix sessions
def get_cities_by_query(*, query):
    Session = sessionmaker(bind=engine)
    session = Session()
    tokens = sub("[\s]+", " ", query).split(",")
    cities = []
    city_unique_ids = set()
    if len(tokens) == 1:
        token = tokens[0]
        state_code = get_state_code(token)
        if state_code is None:  # the call contains only a city name
            encoded_query = sub(" ", "%", query)
            result = session.query(City).filter(
                City.City.ilike("%" + encoded_query + "%")
            )
            for instance in result:
                city_data = {
                    "city": instance.City,
                    "state": instance.State,
                    "quality_of_life_index": instance.QualityOfLifeIndex,
                    "rent_index": instance.RentIndex,
                    "crime_index": instance.CrimeIndex,
                    "image_url": instance.ImageUrl,
                    "population": instance.Population,
                    "area": 123,
                    "traffic_time_index": instance.TrafficTimeIndex,
                }
                city_unique_ids |= {instance.CityId}
                cities.append(city_data)
        else:  # handles matching new york as state and city
            query = state_code + " " + query

    else:  # the call contains city, state
        city, state = tokens[0], tokens[1]
        state_code = get_state_code(state)
        result = session.query(City).filter(
            City.City.ilike("%" + city + "%"), City.State == state_code
        )
        for instance in result:
            city_data = {
                "city": instance.City,
                "state": instance.State,
                "quality_of_life_index": instance.QualityOfLifeIndex,
                "rent_index": instance.RentIndex,
                "crime_index": instance.CrimeIndex,
                "image_url": instance.ImageUrl,
                "population": instance.Population,
                "area": 123,
                "traffic_time_index": instance.TrafficTimeIndex,
            }
            city_unique_ids |= {instance.CityId}
            cities.append(city_data)

    # Single word matching
    query_words = sub("[\s]+", " ", query).split(" ")
    for word in query_words:
        state_code_if_exists = get_state_code(word)
        state_code = word
        if state_code_if_exists is not None:
            state_code = state_code_if_exists

        result = session.query(City).filter(
            or_(
                City.City.ilike("%" + word + "%"),
                City.State.ilike("%" + state_code + "%"),
            )
        )
        for instance in result:
            if instance.CityId not in city_unique_ids:
                city_data = {
                    "city": instance.City,
                    "state": instance.State,
                    "quality_of_life_index": instance.QualityOfLifeIndex,
                    "rent_index": instance.RentIndex,
                    "crime_index": instance.CrimeIndex,
                    "image_url": instance.ImageUrl,
                    "population": instance.Population,
                    "area": 123,
                    "traffic_time_index": instance.TrafficTimeIndex,
                }
                city_unique_ids |= {instance.CityId}
                cities.append(city_data)
    return cities if len(cities) > 0 else None


def get_state_code(state):
    state = state.upper().strip()
    if len(state) == 2:
        return state
    elif len(state) > 2:
        return STATE_NAMES_TO_CODES.get(state, None)


"""
Universities Functions
"""


def get_all_unis():
    Session = sessionmaker(bind=engine)
    session = Session()
    unis = []
    for instance in session.query(University).all():
        uni_data = {
            "university": instance.University,
            "city": instance.City,
            "state": instance.State,
            "cs_ranking": instance.CSRanking,
            "undergrad_population": instance.UndergradPopulation,
            "graduation_rate": instance.GraduationRate,
            "admission_rate": instance.AdmissionRate,
            "image_url": instance.ImageUrl,
        }
        unis.append(uni_data)
    return unis


def get_unis_by_name(*, name, state):
    name = sub("[-,\s]+", "%", name)
    Session = sessionmaker(bind=engine)
    session = Session()
    state_code = get_state_code(state)
    result = session.query(University).filter(
        University.University.ilike("%" + name + "%"), University.State == state_code
    )

    it = iter(result)
    result = None
    try:
        instance = next(it)
    except StopIteration:
        return result

    uni_data = {
        "university": instance.University,
        "city": instance.City,
        "state": instance.State,
        "latitude": instance.Latitude,
        "longitude": instance.Longitude,
        "cs_ranking": instance.CSRanking,
        "undergrad_population": instance.UndergradPopulation,
        "male_percentage": instance.MalePercentage,
        "female_percentage": instance.FemalePercentage,
        "white_percentage": instance.WhitePercentage,
        "black_percentage": instance.BlackPercentage,
        "asian_percentage": instance.AsianPercentage,
        "hispanic_percentage": instance.HispanicPercentage,
        "in_state_tuition": instance.InStateTuition,
        "out_of_state_tuition": instance.OutOfStateTuition,
        "graduation_rate": instance.GraduationRate,
        "admission_rate": instance.AdmissionRate,
        "sat_median_reading": instance.SATMedianReading,
        "sat_median_math": instance.SATMedianMath,
        "act_median_english": instance.ACTMedianEnglish,
        "act_median_math": instance.ACTMedianMath,
        "image_url": instance.ImageUrl,
    }

    result = uni_data
    return result


def get_unis_by_state(*, state):
    Session = sessionmaker(bind=engine)
    session = Session()
    state_code = get_state_code(state)
    result = session.query(University).filter(University.State == state_code)
    unis = []
    for instance in result:
        uni_data = {
            "university": instance.University,
            "city": instance.City,
            "state": instance.State,
            "cs_ranking": instance.CSRanking,
            "undergrad_population": instance.UndergradPopulation,
            "graduation_rate": instance.GraduationRate,
            "admission_rate": instance.AdmissionRate,
            "image_url": instance.ImageUrl,
        }
        unis.append(uni_data)
    return unis if len(unis) > 0 else None


def get_unis_by_city(*, city, state):
    city = sub("[-,\s]+", "%", city)
    Session = sessionmaker(bind=engine)
    session = Session()
    state_code = get_state_code(state)
    result = session.query(University).filter(
        University.City.ilike("%" + city + "%"), University.State == state_code
    )
    unis = []
    for instance in result:
        uni_data = {
            "university": instance.University,
            "city": instance.City,
            "state": instance.State,
            "cs_ranking": instance.CSRanking,
            "undergrad_population": instance.UndergradPopulation,
            "graduation_rate": instance.GraduationRate,
            "admission_rate": instance.AdmissionRate,
            "image_url": instance.ImageUrl,
        }
        unis.append(uni_data)
    return unis if len(unis) > 0 else None


def get_unis_by_query(*, query):
    query = sub("[-,\s]+", "%", query)
    Session = sessionmaker(bind=engine)
    session = Session()
    result = session.query(University).filter(
        University.University.ilike("%" + query + "%")
    )
    unis = []
    uni_unique_ids = set()
    for instance in result:
        uni_data = {
            "university": instance.University,
            "city": instance.City,
            "state": instance.State,
            "cs_ranking": instance.CSRanking,
            "undergrad_population": instance.UndergradPopulation,
            "graduation_rate": instance.GraduationRate,
            "admission_rate": instance.AdmissionRate,
            "image_url": instance.ImageUrl,
        }
        unis.append(uni_data)
        uni_unique_ids |= {instance.UniversityId}

    query_words = query.split("%")
    for word in query_words:
        if word.lower() in {"of", "at", "in", "-"}:
            continue

        state_code_if_exists = get_state_code(word)
        state_code = word
        if state_code_if_exists is not None:
            state_code = state_code_if_exists

        result = session.query(University).filter(
            or_(
                University.University.ilike("%" + word + "%"),
                University.City.ilike("%" + word + "%"),
                University.State.ilike("%" + state_code + "%"),
                University.CSRanking.ilike("%" + word + "%"),
            )
        )
        for instance in result:
            if instance.UniversityId not in uni_unique_ids:
                uni_data = {
                    "university": instance.University,
                    "city": instance.City,
                    "state": instance.State,
                    "cs_ranking": instance.CSRanking,
                    "undergrad_population": instance.UndergradPopulation,
                    "graduation_rate": instance.GraduationRate,
                    "admission_rate": instance.AdmissionRate,
                    "image_url": instance.ImageUrl,
                }
                unis.append(uni_data)
                uni_unique_ids |= {instance.UniversityId}

    return unis if len(unis) > 0 else None


"""
Companies Functions
"""


def get_all_companies():
    Session = sessionmaker(bind=engine)
    session = Session()
    companies = []
    for instance in session.query(Company).all():
        company_data = {
            "company": instance.Company,
            "city": instance.City,
            "state": instance.State,
            "website": instance.Website,
            "size": instance.Size,
            "industry": instance.Industry,
            "year_founded": instance.YearFounded,
            "image_url": instance.ImageUrl,
        }
        companies.append(company_data)
    return companies


def get_companies_by_name(*, name):
    Session = sessionmaker(bind=engine)
    session = Session()
    result = session.query(Company).filter(Company.Company.ilike("%" + name + "%"))
    it = iter(result)
    result = None
    try:
        instance = next(it)
    except StopIteration:
        return result

    company_data = {
        "city": instance.City,
        "state": instance.State,
        "website": instance.Website,
        "company": instance.Company,
        "size": instance.Size,
        "year_founded": instance.YearFounded,
        "company_type": instance.CompanyType,
        "industry": instance.Industry,
        "revenue": instance.Revenue,
        "glassdoor_rating": instance.GlassdoorRating,
        "recommend_to_a_friend": instance.RecommendToAFriend,
        "ceo_approval_rating": instance.CEOApprovalRating,
        "ceo": instance.CEO,
        "positive_interview_experience": instance.PositiveInterviewExperience,
        "neutral_interview_experience": instance.NeutralInterviewExperience,
        "negative_interview_experience": instance.NegativeInterviewExperience,
        "interview_from_online_application": instance.InterviewFromOnlineApplication,
        "interview_from_employee_referral": instance.InterviewFromEmployeeReferral,
        "interview_from_recruiter": instance.InterviewFromRecruiter,
        "interview_difficulty": instance.InterviewDifficulty,
        "image_url": instance.ImageUrl,
    }

    result = company_data
    return result


def get_companies_by_query(*, query):
    Session = sessionmaker(bind=engine)
    session = Session()
    query = sub("[-,\s]+", "%", query)
    result = session.query(Company).filter(Company.Company.ilike("%" + query + "%"))
    companies = []
    comp_unique_ids = set()
    for instance in result:
        company_data = {
            "company": instance.Company,
            "city": instance.City,
            "state": instance.State,
            "website": instance.Website,
            "size": instance.Size,
            "industry": instance.Industry,
            "year_founded": instance.YearFounded,
            "image_url": instance.ImageUrl,
        }
        companies.append(company_data)
        comp_unique_ids |= {instance.CompanyId}

    query_words = query.split("%")
    for word in query_words:

        state_code_if_exists = get_state_code(word)
        state_code = word
        if state_code_if_exists is not None:
            state_code = state_code_if_exists

        result = session.query(Company).filter(
            or_(
                Company.Company.ilike("%" + word + "%"),
                Company.City.ilike("%" + word + "%"),
                Company.State.ilike("%" + state_code + "%"),
                Company.Website.ilike("%" + word + "%"),
                Company.YearFounded.ilike("%" + word + "%"),
                Company.CompanyType.ilike("%" + word + "%"),
                Company.Industry.ilike("%" + word + "%"),
                Company.CEO.ilike("%" + word + "%"),
            )
        )
        for instance in result:
            if instance.CompanyId not in comp_unique_ids:
                company_data = {
                    "company": instance.Company,
                    "city": instance.City,
                    "state": instance.State,
                    "website": instance.Website,
                    "size": instance.Size,
                    "industry": instance.Industry,
                    "year_founded": instance.YearFounded,
                    "image_url": instance.ImageUrl,
                }
                companies.append(company_data)
                comp_unique_ids |= {instance.CompanyId}

    return companies if len(companies) > 0 else None


def get_companies_by_state(*, state):
    Session = sessionmaker(bind=engine)
    session = Session()
    state_code = get_state_code(state)
    result = session.query(Company).filter(Company.State == state_code)
    companies = []
    for instance in result:
        company_data = {
            "company": instance.Company,
            "city": instance.City,
            "state": instance.State,
            "website": instance.Website,
            "size": instance.Size,
            "industry": instance.Industry,
            "year_founded": instance.YearFounded,
            "image_url": instance.ImageUrl,
        }
        companies.append(company_data)
    return companies if len(companies) > 0 else None


def get_companies_by_city(*, city, state):
    Session = sessionmaker(bind=engine)
    session = Session()
    state_code = get_state_code(state)
    result = session.query(Company).filter(
        Company.City.ilike("%" + city + "%"), Company.State == state_code
    )  # Still no state
    companies = []
    for instance in result:
        company_data = {
            "company": instance.Company,
            "city": instance.City,
            "state": instance.State,
            "website": instance.Website,
            "size": instance.Size,
            "industry": instance.Industry,
            "year_founded": instance.YearFounded,
            "image_url": instance.ImageUrl,
        }
        companies.append(company_data)
    return companies if len(companies) > 0 else None
