from werkzeug.exceptions import BadRequest
from flask import Flask, request, Response, json, jsonify
from flask_cors import CORS, cross_origin
import Dbconnection as db

app = Flask(__name__)
app.config["CORS_HEADERS"] = "Content-Type"
CORS(app)


@app.route("/")
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def hello_world():
    return jsonify({"message": "Hello World!"})


@app.route("/user", methods=["GET"])
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def user():
    resp_dict = {"first_name": "Zac", "last_name": "May"}
    return jsonify(resp_dict)


"""
Cities API
"""


@app.route("/cities", methods=["GET"])
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def cities():
    num_params = len(request.args)
    response = []
    if num_params == 0:
        response = db.get_all_cities()
    elif num_params == 1:
        query = request.args.get("q")
        if query == None:
            raise BadRequest("wrong parameters given")

        cities = db.get_cities_by_query(query=query)
        if cities is not None:
            response = cities
    elif num_params == 2:
        city = request.args.get("city")
        state = request.args.get("state")
        if city == None or state == None:
            raise BadRequest("wrong parameters given")

        result = db.get_city(city=city, state=state)
        if result is not None:
            # city_name, city_data = result
            # response['city'] = city_name
            # response['data'] = city_data
            response = result
    else:
        raise BadRequest("unexpected number of parameters given")

    if response is None:
        response = []
    return jsonify(response)


@app.route("/cities/filter", methods=["GET"])
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def cities_by_state():
    num_params = len(request.args)
    response = []
    state = request.args.get("state")
    if num_params == 1 and state != None:
        # make sql call to get all cities matching filter
        cities = db.get_cities_by_state(state=state)
        if cities is not None:
            response = cities
    else:
        raise BadRequest("wrong parameters given")

    if response is None:
        response = []
    return jsonify(response)


"""
Universities API
"""


@app.route("/universities", methods=["GET"])
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def universities():
    num_params = len(request.args)
    response = []
    if num_params == 0:
        response = db.get_all_unis()
    elif num_params == 1:
        query = request.args.get("q")
        if query == None:
            raise BadRequest("wrong parameters given")
        universities = db.get_unis_by_query(query=query)
        response = universities
    elif num_params == 2:
        name = request.args.get("name")
        state = request.args.get("state")
        if name == None or state == None:
            raise BadRequest("wrong parameters given")

        response = db.get_unis_by_name(name=name, state=state)
        # response['data'] = uni_data
    else:
        raise BadRequest("unexpected number of parameters given")
    if response is None:
        response = []
    return jsonify(response)


@app.route("/universities/filter", methods=["GET"])
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def universities_by_filter():
    num_params = len(request.args)
    response = []
    if num_params == 1:
        state = request.args.get("state")
        if state == None:
            raise BadRequest("wrong parameters given")

        # make sql call to get univesities matching filter
        response = db.get_unis_by_state(state=state)
    elif num_params == 2:
        city = request.args.get("city")
        state = request.args.get("state")
        if state == None or city == None:
            raise BadRequest("wrong parameters given")

        # make sql call to get univesities matching filter
        response = db.get_unis_by_city(city=city, state=state)
    else:
        raise BadRequest("unexpected number of parameters given")

    if response is None:
        response = []
    return jsonify(response)


"""
Companies API
"""


@app.route("/companies", methods=["GET"])
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def companies():
    num_params = len(request.args)
    response = []
    if num_params == 0:
        response = db.get_all_companies()
    elif num_params == 1 and request.args.get("q") != None:
        query = request.args.get("q")
        companies = db.get_companies_by_query(
            query=query
        )  # [{'company': 'Apple, Inc', 'city': 'Cupertino'}] # make sql call to get companies matching q
        response = companies

    elif num_params == 1 and request.args.get("name") != None:
        name = request.args.get("name")
        # make sql call to get all info for a company
        response = db.get_companies_by_name(name=name)
    else:
        raise BadRequest("unexpected number of parameters given")

    if response is None:
        response = []
    return jsonify(response)


@app.route("/companies/filter", methods=["GET"])
@cross_origin(origin="*", headers=["Content-Type", "Authorization"])
def companies_by_filter():
    num_params = len(request.args)
    response = []
    if num_params == 1:
        state = request.args.get("state")
        if state == None:
            raise BadRequest("wrong parameters given")

        # make sql call to get companies matching filter
        response = db.get_companies_by_state(state=state)
    elif num_params == 2:
        city = request.args.get("city")
        state = request.args.get("state")
        if state == None or city == None:
            raise BadRequest("wrong parameters given")

        # make sql call to get companies matching filter
        response = db.get_companies_by_city(city=city, state=state)
    else:
        raise BadRequest("unexpected number of parameters given")

    if response is None:
        response = []
    return jsonify(response)


if __name__ == "__main__":
    # for debug: app.run(debug=True, port=5000)
    app.run()
