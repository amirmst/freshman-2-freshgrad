from unittest import main, TestCase
import Dbconnection as db


class UnitTests(TestCase):
    def test_get_all_cities_length(self):
        cities = db.get_all_cities()
        self.assertEqual(len(cities), 100)

    def test_get_all_cities_data(self):
        cities = db.get_all_cities()
        austin = {
            "city": "Austin",
            "state": "TX",
            "quality_of_life_index": 192.73142154138117,
            "rent_index": 51.32436099924931,
            "crime_index": 35.8035748116862,
            "image_url": "https://i.ytimg.com/vi/0eiGy63BLKU/maxresdefault.jpg",
            "population": 678368.0,
            "area": 123,
            "traffic_time_index": 34.342480136986296,
        }
        las_vegas = {
            "city": "Las Vegas",
            "state": "NV",
            "quality_of_life_index": 192.73142154138117,
            "rent_index": 56.32436099924931,
            "crime_index": 32.8035748116862,
            "image_url": "https://i.ytimg.com/vi/0eiGy63BLKU/maxresdefault.jpg",
            "population": 678368.0,
            "area": 120,
            "traffic_time_index": 34.342480136986296,
        }
        self.assertEqual(austin in cities, True)
        self.assertEqual(las_vegas in cities, False)

    def test_get_cities_by_state_length(self):
        cities = db.get_cities_by_state(state="TX")
        self.assertEqual(len(cities), 6)

    def test_get_cities_by_state_data(self):
        cities = db.get_cities_by_state(state="texas")
        austin = {
            "area": 123,
            "city": "Austin",
            "crime_index": 35.8035748116862,
            "image_url": "https://i.ytimg.com/vi/0eiGy63BLKU/maxresdefault.jpg",
            "population": 678368.0,
            "quality_of_life_index": 192.73142154138117,
            "rent_index": 51.32436099924931,
            "state": "TX",
            "traffic_time_index": 34.342480136986296,
        }
        states = set()
        for city in cities:
            states.add(city["state"])
        self.assertEqual(len(states), 1)
        self.assertEqual(next(iter(states)), "TX")
        self.assertEqual(austin in cities, True)

    def test_get_cities_by_state_none(self):
        cities = db.get_cities_by_state(state="ZZ")
        self.assertIsNone(cities)

    def test_get_city_args_length(self):
        cities = db.get_city(city="Austin", state="TX")
        self.assertEqual(len(cities), 12)

    def test_get_city_data(self):
        city = db.get_city(city="Austin", state="Texas")
        expected_data = {
            "city": "Austin",
            "state": "TX",
            "latitude": 30.267153,
            "longitude": -97.7430608,
            "crime_index": 35.8035748116862,
            "health_care_index": 70.70297309103279,
            "traffic_time_index": 34.342480136986296,
            "quality_of_life_index": 192.73142154138117,
            "cpi_index": 67.39940182506115,
            "rent_index": 51.32436099924931,
            "population": 678368.0,
            "image_url": "https://i.ytimg.com/vi/0eiGy63BLKU/maxresdefault.jpg",
        }
        self.assertEqual(city, expected_data)

    def test_get_city_args_none(self):
        cities = db.get_city(city="ABC", state="TX")
        self.assertIsNone(cities)

    def test_get_cities_by_query_none(self):
        cities = db.get_cities_by_query(query="Austintx")
        self.assertIsNone(cities)

    def test_get_cities_by_query_length(self):
        cities = db.get_cities_by_query(query="Austin,tx")
        self.assertEqual(len(cities), 4)
        self.assertTrue(cities[0]["city"] == "Austin")

    def test_get_cities_by_query_data(self):
        cities = db.get_cities_by_query(query="Austin,tx")
        expected = {
            "area": 123,
            "city": "Austin",
            "crime_index": 35.8035748116862,
            "image_url": "https://i.ytimg.com/vi/0eiGy63BLKU/maxresdefault.jpg",
            "population": 678368.0,
            "quality_of_life_index": 192.73142154138117,
            "rent_index": 51.32436099924931,
            "state": "TX",
            "traffic_time_index": 34.342480136986296,
        }
        self.assertIn(expected, cities)

    def test_get_cities_by_query_length(self):
        cities = db.get_cities_by_query(query="New York")
        nyc = {
            "city": "New York",
            "state": "NY",
            "quality_of_life_index": 142.42124586826878,
            "rent_index": 100.0,
            "crime_index": 44.79790090031839,
            "image_url": "https://i.ytimg.com/vi/Fl7V5dX5IrM/maxresdefault.jpg",
            "population": 8107916.0,
            "area": 123,
            "traffic_time_index": 42.74453125000001,
        }
        self.assertIn(nyc, cities)
        self.assertEqual(len(cities), 11)

    def test_get_state_code(self):
        self.assertEqual(db.get_state_code("Texas"), "TX")
        self.assertEqual(db.get_state_code("California"), "CA")
        self.assertEqual(db.get_state_code("ca"), "CA")
        self.assertIsNone(db.get_state_code("NOTASTATE"))

    def test_get_all_unis_length(self):
        unis = db.get_all_unis()
        self.assertEqual(len(unis), 100)

    def test_get_all_unis_data(self):
        unis = db.get_all_unis()
        ut = {
            "university": "University of Texas at Austin",
            "city": "Austin",
            "state": "TX",
            "cs_ranking": 10,
            "undergrad_population": 39965,
            "graduation_rate": 0.8029,
            "admission_rate": 36.49,
            "image_url": "https://upload.wikimedia.org/wikipedia/commons/7/78/University_of_Texas_at_Austin.JPG",
        }
        not_a_uni = {
            "university": "SWE University",
            "city": "Austin",
            "state": "TX",
            "cs_ranking": 10,
            "undergrad_population": 39965,
            "graduation_rate": 0.8029,
            "admission_rate": 36.49,
            "image_url": "https://upload.wikimedia.org/wikipedia/commons/7/78/University_of_Texas_at_Austin.JPG",
        }
        self.assertIn(ut, unis)
        self.assertNotIn(not_a_uni, unis)

    def test_get_uni_by_name_data(self):
        uni = db.get_unis_by_name(name="Harvard", state="MA")
        expected = {
            "university": "Harvard University",
            "city": "Cambridge",
            "state": "MA",
            "latitude": 42.37588000000005,
            "longitude": -71.12339999999995,
            "cs_ranking": 18,
            "undergrad_population": 7532,
            "male_percentage": 52.31,
            "female_percentage": 47.69,
            "white_percentage": 42.11,
            "black_percentage": 7.32,
            "asian_percentage": 19.42,
            "hispanic_percentage": 11.600000000000001,
            "in_state_tuition": 48949,
            "out_of_state_tuition": 48949,
            "graduation_rate": 0.9773,
            "admission_rate": 5.16,
            "sat_median_reading": 760,
            "sat_median_math": 765,
            "act_median_english": 35,
            "act_median_math": 33,
            "image_url": "http://4.bp.blogspot.com/-jwOwQPVG0IM/TpvE8U_3bGI/AAAAAAAABDQ/Q_4Pzr2X0G4/s1600/harvard+university+%25282%2529.jpg",
        }
        self.assertDictEqual(expected, uni)

    def test_get_unis_by_state_length(self):
        unis = db.get_unis_by_state(state="TX")
        self.assertEqual(len(unis), 5)

    def test_get_unis_by_state_data(self):
        unis = db.get_unis_by_state(state="TX")
        expected = {
            "admission_rate":36.49,
            "city":"Austin",
            "cs_ranking":10,
            "graduation_rate":0.8029,
            "image_url":"https://upload.wikimedia.org/wikipedia/commons/7/78/University_of_Texas_at_Austin.JPG",
            "state":"TX",
            "undergrad_population":39965,
            "university":"University of Texas at Austin"
        }
        self.assertTrue(expected in unis)

    def test_get_unis_by_state_none(self):
        unis = db.get_unis_by_state(state="ZZ")
        self.assertIsNone(unis)

    def test_get_unis_by_city_length(self):
        unis = db.get_unis_by_city(city="Austin", state="TX")
        self.assertEqual(len(unis), 1)

    def test_get_unis_by_city_data(self):
        unis = db.get_unis_by_city(city="Austin", state="TX")
        expected = {
            "admission_rate":36.49,
            "city":"Austin",
            "cs_ranking":10,
            "graduation_rate":0.8029,
            "image_url":"https://upload.wikimedia.org/wikipedia/commons/7/78/University_of_Texas_at_Austin.JPG",
            "state":"TX",
            "undergrad_population":39965,
            "university":"University of Texas at Austin"
        }
        self.assertEqual(expected, unis[0])

    def test_get_unis_by_city_none(self):
        unis = db.get_unis_by_city(city="ABC", state="TX")
        self.assertIsNone(unis)

    def test_get_unis_by_query(self):
        unis = db.get_unis_by_query(query="University of Texas")
        expected = [
            {
                "university": "University of Texas at Austin",
                "city": "Austin",
                "state": "TX",
                "cs_ranking": 10,
                "undergrad_population": 39965,
                "graduation_rate": 0.8029,
                "admission_rate": 36.49,
                "image_url": "https://upload.wikimedia.org/wikipedia/commons/7/78/University_of_Texas_at_Austin.JPG",
            },
            {
                "university": "University of Texas at Dallas",
                "city": "Richardson",
                "state": "TX",
                "cs_ranking": 80,
                "undergrad_population": 2663,
                "graduation_rate": None,
                "admission_rate": 73.97,
                "image_url": "http://2011.igem.org/wiki/images/b/bf/NSERL_Building_2.jpg",
            },
            {
                "university": "University of Texas at Arlington",
                "city": "Arlington",
                "state": "TX",
                "cs_ranking": 99,
                "undergrad_population": 32955,
                "graduation_rate": 0.5574,
                "admission_rate": 71.22,
                "image_url": "http://www.byuaccounting.net/mediawiki/images/6/6e/University-of-Texas-Arlington-Print-C10084982.jpg",
            },
        ]
        self.assertTrue(all(uni in unis for uni in expected))
        self.assertEqual(len(unis), 89)

    def test_get_unis_by_query_none(self):
        unis = db.get_unis_by_query(query="NotAUni")
        self.assertIsNone(unis)

    def test_get_all_companies_length(self):
        companies = db.get_all_companies()
        self.assertEqual(len(companies), 105)

    def test_get_all_companies_data(self):
        companies = db.get_all_companies()
        solar_winds = {
            "city": "Austin",
            "company": "SolarWinds",
            "image_url": "https://media.glassdoor.com/sql/100286/solarwinds-squarelogo-1510164600122.png",
            "industry": "Enterprise Software & Network Solutions",
            "size": "1001 to 5000 employees",
            "state": "TX",
            "website": "www.solarwinds.com",
            "year_founded": 1999,
        }

        not_a_comp = {
            "city": "Austin",
            "company": "not_a_comp",
            "image_url": "https://media.glassdoor.com/sql/100286/solarwinds-squarelogo-1510164600122.png",
            "industry": "Enterprise Software & Network Solutions",
            "size": "1001 to 5000 employees",
            "state": "TX",
            "website": "www.solarwinds.com",
            "year_founded": 1999,
        }
        self.assertIn(solar_winds, companies)
        self.assertNotIn(not_a_comp, companies)

    def test_get_company_by_name_data(self):
        company = db.get_companies_by_name(name="Apple")
        expected = {
            "ceo": "Tim Cook",
            "ceo_approval_rating": "91%",
            "city": "Cupertino",
            "company": "Apple",
            "company_type": "Company - Public (AAPL)",
            "glassdoor_rating": 4.0,
            "image_url": "https://media.glassdoor.com/sql/1138/apple-squarelogo-1485292862952.png",
            "industry": "Computer Hardware & Software",
            "interview_difficulty": "Average",
            "interview_from_employee_referral": "15%",
            "interview_from_online_application": "56%",
            "interview_from_recruiter": "12%",
            "negative_interview_experience": "15%",
            "neutral_interview_experience": "20%",
            "positive_interview_experience": "65%",
            "recommend_to_a_friend": "80%",
            "revenue": "$10+ billion (USD) per year",
            "size": "10000+ employees",
            "state": "CA",
            "website": "www.apple.com",
            "year_founded": 1976,
        }
        self.assertDictEqual(expected, company)

    def test_get_companies_by_state_length(self):
        companies = db.get_companies_by_state(state="TX")
        self.assertEqual(len(companies), 8)

    def test_get_companies_by_state_data(self):
        companies = db.get_companies_by_state(state="TX")
        expected = [
            "SolarWinds",
            "Valero Energy",
            "ExxonMobil",
            "Dell Technologies",
            "AT&T",
            "Phillips 66",
            "McKesson",
            "Epicor Software",
        ]
        self.assertTrue(all(company['company'] in expected for company in companies))

    def test_get_companies_by_state_none(self):
        companies = db.get_companies_by_state(state="ZZ")
        self.assertIsNone(companies)

    def test_get_companies_by_city_length(self):
        companies = db.get_companies_by_city(city="Austin", state="TX")
        self.assertEqual(len(companies), 2)

    def test_get_companies_by_city_data(self):
        companies = db.get_companies_by_city(city="Austin", state="TX")
        expected = ["SolarWinds", "Epicor Software"]
        self.assertTrue(all(company['company'] in expected for company in companies))

    def test_get_companies_by_city_none(self):
        companies = db.get_companies_by_city(city="ABC", state="TX")
        self.assertIsNone(companies)

    def test_get_companies_by_query(self):
        companies = db.get_companies_by_query(query="CA")
        expected = [
            {
                "company": "CA Technologies",
                "city": "San Jose",
                "state": "CA",
                "website": "www.broadcom.com",
                "size": "10000+ employees",
                "industry": "Computer Hardware & Software",
                "year_founded": 1976,
                "image_url": "https://media.glassdoor.com/sql/167/ca-technologies-squarelogo-1403882544970.png",
            },
            {
                "company": "Cardinal Health",
                "city": "Dublin",
                "state": "OH",
                "website": "www.cardinalhealth.com",
                "size": "10000+ employees",
                "industry": "Wholesale",
                "year_founded": 1971,
                "image_url": "https://media.glassdoor.com/sql/1227/cardinal-health-squarelogo-1391194404797.png",
            },
        ]
        self.assertTrue(all(company in companies for company in expected))
        self.assertEqual(len(companies), 52)

    def test_get_companies_by_query_none(self):
        companies = db.get_companies_by_query(query="NotAComp")
        self.assertIsNone(companies)


if __name__ == "__main__":
    main()
