from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float

Base = declarative_base()

# mapping state names to their two letter codes
STATE_NAMES_TO_CODES = {
    "ALASKA": "AK",
    "ALABAMA": "AL",
    "ARKANSAS": "AR",
    "AMERICAN": "AS",
    "ARIZONA": "AZ",
    "CALIFORNIA": "CA",
    "COLORADO": "CO",
    "CONNECTICUT": "CT",
    "DISTRICT": "DC",
    "DELAWARE": "DE",
    "FLORIDA": "FL",
    "GEORGIA": "GA",
    "GUAM": "GU",
    "HAWAII": "HI",
    "IOWA": "IA",
    "IDAHO": "ID",
    "ILLINOIS": "IL",
    "INDIANA": "IN",
    "KANSAS": "KS",
    "KENTUCKY": "KY",
    "LOUISIANA": "LA",
    "MASSACHUSETTS": "MA",
    "MARYLAND": "MD",
    "MAINE": "ME",
    "MICHIGAN": "MI",
    "MINNESOTA": "MN",
    "MISSOURI": "MO",
    "MISSISSIPPI": "MS",
    "MONTANA": "MT",
    "NORTH CAROLINA": "NC",
    "NORTH DACOTA": "ND",
    "NEBRASKA": "NE",
    "NEW HAMPSHIRE": "NH",
    "NEW JERSEY": "NJ",
    "NEW MEXICO": "NM",
    "NEVADA": "NV",
    "NEW YORK": "NY",
    "OHIO": "OH",
    "OKLAHOMA": "OK",
    "OREGON": "OR",
    "PENNSYLVANIA": "PA",
    "PUERTO": "PR",
    "RHODE": "RI",
    "SOUTH CAROLINA": "SC",
    "SOUTH DACOTA": "SD",
    "TENNESSEE": "TN",
    "TEXAS": "TX",
    "UTAH": "UT",
    "VIRGINIA": "VA",
    "VIRGIN": "VI",
    "VERMONT": "VT",
    "WASHINGTON": "WA",
    "WISCONSIN": "WI",
    "WEST": "WV",
    "WYOMING": "WY",
}


class City(Base):
    __tablename__ = "City"

    CityId = Column(Integer, primary_key=True)
    City = Column(String)
    State = Column(String)
    Latitude = Column(Float)
    Longitude = Column(Float)
    CrimeIndex = Column(Float)
    HealthCareIndex = Column(Float)
    TrafficTimeIndex = Column(Float)
    QualityOfLifeIndex = Column(Float)
    CPIIndex = Column(Float)
    RentIndex = Column(Float)
    Population = Column(Float)
    ImageUrl = Column(String)


class University(Base):
    __tablename__ = "University"

    UniversityId = Column(Integer, primary_key=True)
    University = Column(String)
    City = Column(String)
    State = Column(String)
    CityId = Column(Integer)
    Latitude = Column(Float)
    Longitude = Column(Float)
    CSRanking = Column(Integer)
    UndergradPopulation = Column(Integer)
    MalePercentage = Column(Float)
    FemalePercentage = Column(Float)
    WhitePercentage = Column(Float)
    BlackPercentage = Column(Float)
    AsianPercentage = Column(Float)
    HispanicPercentage = Column(Float)
    InStateTuition = Column(Integer)
    OutOfStateTuition = Column(Integer)
    GraduationRate = Column(Float)
    AdmissionRate = Column(Float)
    SATMedianReading = Column(Integer)
    SATMedianMath = Column(Integer)
    ACTMedianEnglish = Column(Integer)
    ACTMedianMath = Column(Integer)
    ImageUrl = Column(String)


class Company(Base):
    __tablename__ = "Company"

    CompanyId = Column(Integer, primary_key=True)
    Company = Column(String)
    City = Column(String)
    CityId = Column(Integer)
    State = Column(String)
    Website = Column(String)
    Size = Column(String)
    YearFounded = Column(Integer)
    CompanyType = Column(String)
    Industry = Column(String)
    Revenue = Column(String)
    GlassdoorRating = Column(String)
    RecommendToAFriend = Column(String)
    CEOApprovalRating = Column(String)
    CEO = Column(String)
    PositiveInterviewExperience = Column(String)
    NeutralInterviewExperience = Column(String)
    NegativeInterviewExperience = Column(String)
    InterviewFromOnlineApplication = Column(String)
    InterviewFromEmployeeReferral = Column(String)
    InterviewFromRecruiter = Column(String)
    InterviewDifficulty = Column(String)
    ImageUrl = Column(String)
