from sys import stdin, stdout
import requests
import csv
import time

UNIS = []
RANKINGS = []
CITIES = []
POIS = []

LATS = []
LONGS = []

REQUEST_URL = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?f=json&singleLine="
TOKEN = "vm_SDLuuYCkqRmVxXUkkswBciubrz9MaHPuSejjXuNe3MeGU7sHMTYOv-mzS1LRTy1mdsR9U0dI8Nr_Pcdogj0bkYezivd8XM1vjuPG4qG_JKw9-6w2FHG9_I5SclkiE"


def pre_process(file_to_read):
    with open(file_to_read) as csv_file:
        ranking = 1
        csv_reader = csv.reader(csv_file, delimiter=",")
        for row in csv_reader:
            uni = row[0].strip()
            city = row[1].strip()
            state = row[2].strip()
            poi = uni + ", " + city + ", " + state
            UNIS.append(uni)
            RANKINGS.append(ranking)
            CITIES.append(city)
            POIS.append(poi)
            ranking += 1
            print(city + ",", state)  # pipe into a new file


def get_lat_longs():
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    for poi in POIS:
        payload_poi = poi.replace(" ", "%20")
        payload_poi = poi.replace(",", "%2C")
        url = REQUEST_URL + poi
        payload = "f=json&singleLine=" + payload_poi
        response = requests.request("GET", url, headers=headers, data=payload)
        location_data = response.json()["candidates"][0]["location"]
        lon = location_data["x"]
        lat = location_data["y"]
        LATS.append(lat)
        LONGS.append(lon)
        time.sleep(1.5)


def write_csv(file_name):
    with open(file_name, "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(["University", "Latitude", "Longitude", "City", "CS Ranking"])
        for i in range(len(UNIS)):
            writer.writerow([UNIS[i], LATS[i], LONGS[i], CITIES[i], RANKINGS[i]])


if __name__ == "__main__":
    pre_process("unis-cities-states.csv")
    get_lat_longs()
    # write_csv('unis_model.csv')
