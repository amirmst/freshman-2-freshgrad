import requests
import urllib.parse
import csv


BASE_URL = "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="

payload = {}
headers = {"Ocp-Apim-Subscription-Key": "7510405455af4dcb854710ae70eef401"}

with open("unis_model_no_img.csv") as read_obj, open(
    "unis_model.csv", "w", newline=""
) as write_obj:
    writer = csv.writer(write_obj)
    csv_reader = csv.reader(read_obj, delimiter=",")
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            row.append("Image URL")
            writer.writerow(row)
            line_count += 1
            continue
        else:
            uni_name = row[0]
            query = urllib.parse.quote(uni_name)
            response = requests.request(
                "GET", BASE_URL + query, headers=headers, data=payload
            ).json()
            image_url = response["value"][0]["contentUrl"]
            row.append(image_url)
            writer.writerow(row)
            print(image_url)
