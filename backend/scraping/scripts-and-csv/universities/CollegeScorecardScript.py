import requests
import urllib.parse
import csv

API_KEY = "ZQugJS6UcCkaUTMe2b0FqaycpaOdpYukBoqiDD0o"
BASE_URL = "https://api.data.gov/ed/collegescorecard/v1/schools"


with open("unis_model_no_demographics.csv") as read_obj, open(
    "unis_model_1.csv", "w", newline=""
) as write_obj:
    writer = csv.writer(write_obj)
    csv_reader = csv.reader(read_obj, delimiter=",")
    writer.writerow(
        [
            "University",
            "Latitude",
            "Longitude",
            "City",
            "CS Ranking",
            "Undergrad Population",
            "Male Percentage",
            "Female Percentage",
            "White Percentage",
            "Black Percentage",
            "Asian Percentage",
            "Hispanic Percentage",
            "In-State Tuition",
            "Out of State Tuition",
            "Graduation Rate",
            "Admission Rate",
            "SAT Median Reading",
            "SAT Median Math",
            "ACT Median English",
            "ACT Median Math",
        ]
    )
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
            continue
        else:
            uni_name = row[0]
            uni_name_encoded = urllib.parse.quote(uni_name)
            request_params = "?school.name=" + uni_name_encoded + "&api_key=" + API_KEY
            response = requests.request("GET", BASE_URL + request_params)
            try:
                # For Washington State, get [2] instead of [0]
                results = response.json()["results"][0]
                id = results["id"]
                latest_info = results["latest"]
                latest_student_info = latest_info["student"]
                admission_info = latest_info["admissions"]
                admission_rate = admission_info["admission_rate"]["overall"]
                admission_rate = (
                    "" if admission_rate == None else float(admission_rate) * 100
                )
                size = latest_student_info["size"]
                demographics_info = latest_student_info["demographics"]
                men_percentage = float(demographics_info["men"]) * 100
                female_percentage = 100 - men_percentage
                race_ethnicity_info = demographics_info["race_ethnicity"]
                black_percentage = float(race_ethnicity_info["black"]) * 100
                asian_percentage = float(race_ethnicity_info["asian"]) * 100
                white_percentage = float(race_ethnicity_info["white"]) * 100
                hispanic_percentage = float(race_ethnicity_info["hispanic"]) * 100
                in_state_tuition = latest_info["cost"]["tuition"]["in_state"]
                out_of_state_tuition = latest_info["cost"]["tuition"]["out_of_state"]
                grad_rate = latest_info["completion"]["consumer_rate"]
                sat_math_median = admission_info["sat_scores"]["midpoint"]["math"]
                sat_reading_median = admission_info["sat_scores"]["midpoint"][
                    "critical_reading"
                ]
                act_math_median = admission_info["act_scores"]["midpoint"]["math"]
                act_english_median = admission_info["act_scores"]["midpoint"]["english"]
                writer.writerow(
                    [
                        uni_name,
                        row[1],
                        row[2],
                        row[3],
                        row[4],
                        size,
                        men_percentage,
                        female_percentage,
                        white_percentage,
                        black_percentage,
                        asian_percentage,
                        hispanic_percentage,
                        in_state_tuition,
                        out_of_state_tuition,
                        grad_rate,
                        admission_rate,
                        sat_reading_median,
                        sat_math_median,
                        act_english_median,
                        act_math_median,
                    ]
                )

                print(uni_name, response.json()["results"][0]["id"])
            except Exception as e:
                print("*****", uni_name)
                print(e)
