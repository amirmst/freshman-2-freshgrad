# TOTPOP_CY, TOTMALES, TOTFEMALES, DIVINDX_CY, AVGVAL_CY, AVGVAL_FY
import requests
import csv
import time


TOKEN = "vm_SDLuuYCkqRmVxXUkkswBciubrz9MaHPuSejjXuNe3MeGU7sHMTYOv-mzS1LRTy1mdsR9U0dI8Nr_Pcdogj0bkYezivd8XM1vjuPG4qG_JKw9-6w2FHG9_I5SclkiE"
BASE_URL = "https://geoenrich.arcgis.com/arcgis/rest/services/World/geoenrichmentserver/GeoEnrichment/enrich"

with open("unis_model_no_demographics.csv") as read_obj, open(
    "unis_model.csv", "w", newline=""
) as write_obj:
    writer = csv.writer(write_obj)
    writer.writerow(
        [
            "University",
            "Latitude",
            "Longitude",
            "City",
            "CS Ranking",
            "Total Population",
            "Male Population",
            "Female Population",
            "Diversity Index",
        ]
    )
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    csv_reader = csv.reader(read_obj, delimiter=",")
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            line_count += 1
            continue
        else:
            x = row[2]
            y = row[1]
            study_areas_encoded = (
                "%5B%7B%22geometry%22%3A%7B%22x%22%3A"
                + x
                + "%2C%22y%22%3A"
                + y
                + "%7D%7D%5D"
            )
            payload = (
                "f=json&token="
                + TOKEN
                + "&inSR=4326&outSR=4326&studyAreas="
                + study_areas_encoded
                + "&studyAreasOptions=%7B%22areaType%22%3A%22RingBuffer%22%2C%22bufferUnits%22%3A%22esriMiles%22%2C%22bufferRadii%22%3A%5B2%5D%7D&dataCollections=%5B%22KeyGlobalFacts%22%2C%20%22KeyUSFacts%22%5D"
            )
            response = requests.request("POST", BASE_URL, headers=headers, data=payload)
            attributes = response.json()["results"][0]["value"]["FeatureSet"][0][
                "features"
            ][0]["attributes"]
            total_pop = attributes["TOTPOP_CY"]
            male_pop = attributes["TOTMALES"]
            female_pop = attributes["TOTFEMALES"]
            div_index = attributes["DIVINDX_CY"]

            # want these for uni? I don't think so
            avg_current_home_val = attributes["AVGVAL_CY"]
            avg_future_home_val = attributes["AVGVAL_FY"]

            writer.writerow(
                [
                    row[0],
                    row[1],
                    row[2],
                    row[3],
                    row[4],
                    total_pop,
                    male_pop,
                    female_pop,
                    div_index,
                ]
            )
            time.sleep(1.2)
