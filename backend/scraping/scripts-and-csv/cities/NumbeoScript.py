import requests
import csv
import urllib.parse

"""
Missing cities
****** La Jolla, CA ******
****** University Park, PA ******
****** Piscataway, NJ ******
****** Stony Brook, NY ******
****** Notre Dame, IN ******
****** Richardson, TX ******
****** Waltham, MA ******
****** Clemson, SC ******
****** Storrs, CT ******
****** Provo, UT ******
"""

API_KEY = "jte09hyxdpkw67"
CITIES_URL = "https://www.numbeo.com/api/cities?api_key=" + API_KEY
INDICES_URL = "https://www.numbeo.com/api/indices?api_key=" + API_KEY + "&city_id="

my_cities = set()

with open("cities_names.csv") as read_obj:
    csv_reader = csv.reader(read_obj, delimiter=",")
    for row in csv_reader:
        my_cities |= {row[0].strip() + ", " + row[1].strip()}


response = requests.get(CITIES_URL).json()["cities"]

filtered_response = filter(lambda city: city["city"] in my_cities, response)

cities_stored = list(filtered_response)


with open("cities_model.csv", "w", newline="") as write_obj:
    writer = csv.writer(write_obj)
    writer.writerow(
        [
            "City",
            "State",
            "Latitude",
            "Longitude",
            "Crime Index",
            "Health Care Index",
            "Traffic Time Index",
            "Quality of Life Index",
            "CPI Index",
            "Rent Index",
        ]
    )
    for city in cities_stored:
        city_id = city["city_id"]
        city_lat = city["latitude"]
        city_lon = city["longitude"]
        request_url = INDICES_URL + str(city_id)
        response = requests.get(request_url).json()
        try:
            city, state, country = response["name"].split(", ")
            print(city, state, country)
            health_care_index = response.get("health_care_index", "")
            crime_index = response.get("crime_index", "")
            traffic_time_index = response.get("traffic_time_index", "")
            quality_of_life_index = response.get("quality_of_life_index", "")
            cpi_index = response.get("cpi_index", "")
            rent_index = response.get("rent_index", "")
            writer.writerow(
                [
                    city.strip(),
                    state.strip(),
                    city_lat,
                    city_lon,
                    crime_index,
                    health_care_index,
                    traffic_time_index,
                    quality_of_life_index,
                    cpi_index,
                    rent_index,
                ]
            )
        except Exception as e:
            print(e)
            print("***** Missing *****", city)
