import requests
import json
import csv


def get_city_opendata(city, country):
    tmp = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=worldcitiespop&q=%s&sort=population&facet=country&refine.country=%s"
    cmd = tmp % (city, country)
    res = requests.get(cmd)
    dct = json.loads(res.content)
    out = 0
    try:
        out = dct["records"][0]["fields"]["population"]
    except Exception as e:
        print("call failed for:", city)

    return out


with open("cities_model.csv") as read_obj, open(
    "cities_model_with_population.csv", "w", newline=""
) as write_obj:
    writer = csv.writer(write_obj)
    csv_reader = csv.reader(read_obj, delimiter=",")
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            row.append("population")
            writer.writerow(row)
            line_count += 1
            continue
        else:
            city_name = row[0] + ", " + row[1]
            population = get_city_opendata(city_name, "us")
            row.append(population)
            writer.writerow(row)
