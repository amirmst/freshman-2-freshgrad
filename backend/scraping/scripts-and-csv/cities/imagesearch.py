import requests
import urllib.parse
import csv


BASE_URL = "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="

payload = {}
headers = {"Ocp-Apim-Subscription-Key": "7510405455af4dcb854710ae70eef401"}

with open("cities_model_no_img.csv") as read_obj, open(
    "cities_model.csv", "w", newline=""
) as write_obj:
    writer = csv.writer(write_obj)
    csv_reader = csv.reader(read_obj, delimiter=",")
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            row.append("Image URL")
            writer.writerow(row)
            line_count += 1
            continue
        else:
            city_name = row[0] + ", " + row[1] + " Skyline"
            query = urllib.parse.quote(city_name)
            response = requests.request(
                "GET", BASE_URL + query, headers=headers, data=payload
            ).json()
            image_url = response["value"][0]["contentUrl"]
            row.append(image_url)
            writer.writerow(row)
            print(image_url)
