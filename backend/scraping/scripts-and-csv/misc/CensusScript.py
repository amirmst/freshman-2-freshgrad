#!/usr/bin/env python3

from sys import stdin, stdout
import requests
import csv

"""
example API call and response
https://api.census.gov/data/2018/acs/acs1/cprofile?get=CP05_2018_035E&for=STATE:*&key=API_KEY

"""
API_KEY = "ffe5236a18907268ea57ed082c6192f29757be79"

# 3 states for PHASE I
TRIAL_STATES = [
    "TEXAS",
    "CALIFORNIA",
    "WASHINGTON",
]

# mapping two letter state codes to their full name
STATE_CODES_TO_NAMES = {
    "AK": "ALASKA",
    "AL": "ALABAMA",
    "AR": "ARKANSAS",
    "AS": "AMERICAN",
    "AZ": "ARIZONA",
    "CA": "CALIFORNIA",
    "CO": "COLORADO",
    "CT": "CONNECTICUT",
    "DC": "DISTRICT",
    "DE": "DELAWARE",
    "FL": "FLORIDA",
    "GA": "GEORGIA",
    "GU": "GUAM",
    "HI": "HAWAII",
    "IA": "IOWA",
    "ID": "IDAHO",
    "IL": "ILLINOIS",
    "IN": "INDIANA",
    "KS": "KANSAS",
    "KY": "KENTUCKY",
    "LA": "LOUISIANA",
    "MA": "MASSACHUSETTS",
    "MD": "MARYLAND",
    "ME": "MAINE",
    "MI": "MICHIGAN",
    "MN": "MINNESOTA",
    "MO": "MISSOURI",
    "MS": "MISSISSIPPI",
    "MT": "MONTANA",
    "NC": "NORTH CAROLINA",
    "ND": "NORTH DACOTA",
    "NE": "NEBRASKA",
    "NH": "NEW HAMPSHIRE",
    "NJ": "NEW JERSEY",
    "NM": "NEW MEXICO",
    "NV": "NEVADA",
    "NY": "NEW YORK",
    "OH": "OHIO",
    "OK": "OKLAHOMA",
    "OR": "OREGON",
    "PA": "PENNSYLVANIA",
    "PR": "PUERTO",
    "RI": "RHODE",
    "SC": "SOUTH CAROLINA",
    "SD": "SOUTH DACOTA",
    "TN": "TENNESSEE",
    "TX": "TEXAS",
    "UT": "UTAH",
    "VA": "VIRGINIA",
    "VI": "VIRGIN",
    "VT": "VERMONT",
    "WA": "WASHINGTON",
    "WI": "WISCONSIN",
    "WV": "WEST",
    "WY": "WYOMING",
}

# mapping state names to their two letter codes
STATE_NAMES_TO_CODES = {
    "ALASKA": "AK",
    "ALABAMA": "AL",
    "ARKANSAS": "AR",
    "AMERICAN": "AS",
    "ARIZONA": "AZ",
    "CALIFORNIA": "CA",
    "COLORADO": "CO",
    "CONNECTICUT": "CT",
    "DISTRICT": "DC",
    "DELAWARE": "DE",
    "FLORIDA": "FL",
    "GEORGIA": "GA",
    "GUAM": "GU",
    "HAWAII": "HI",
    "IOWA": "IA",
    "IDAHO": "ID",
    "ILLINOIS": "IL",
    "INDIANA": "IN",
    "KANSAS": "KS",
    "KENTUCKY": "KY",
    "LOUISIANA": "LA",
    "MASSACHUSETTS": "MA",
    "MARYLAND": "MD",
    "MAINE": "ME",
    "MICHIGAN": "MI",
    "MINNESOTA": "MN",
    "MISSOURI": "MO",
    "MISSISSIPPI": "MS",
    "MONTANA": "MT",
    "NORTH CAROLINA": "NC",
    "NORTH DACOTA": "ND",
    "NEBRASKA": "NE",
    "NEW HAMPSHIRE": "NH",
    "NEW JERSEY": "NJ",
    "NEW MEXICO": "NM",
    "NEVADA": "NV",
    "NEW YORK": "NY",
    "OHIO": "OH",
    "OKLAHOMA": "OK",
    "OREGON": "OR",
    "PENNSYLVANIA": "PA",
    "PUERTO": "PR",
    "RHODE": "RI",
    "SOUTH CAROLINA": "SC",
    "SOUTH DACOTA": "SD",
    "TENNESSEE": "TN",
    "TEXAS": "TX",
    "UTAH": "UT",
    "VIRGINIA": "VA",
    "VIRGIN": "VI",
    "VERMONT": "VT",
    "WASHINGTON": "WA",
    "WISCONSIN": "WI",
    "WEST": "WV",
    "WYOMING": "WY",
}

# mapping two letter state codes to their FIPS code
STATE_FIPS = {
    "AK": "02",
    "AL": "01",
    "AR": "05",
    "AS": "60",
    "AZ": "04",
    "CA": "06",
    "CO": "08",
    "CT": "09",
    "DC": "11",
    "DE": "10",
    "FL": "12",
    "GA": "13",
    "GU": "66",
    "HI": "15",
    "IA": "19",
    "ID": "16",
    "IL": "17",
    "IN": "18",
    "KS": "20",
    "KY": "21",
    "LA": "22",
    "MA": "25",
    "MD": "24",
    "ME": "23",
    "MI": "26",
    "MN": "27",
    "MO": "29",
    "MS": "28",
    "MT": "30",
    "NC": "37",
    "ND": "38",
    "NE": "31",
    "NH": "33",
    "NJ": "34",
    "NM": "35",
    "NV": "32",
    "NY": "36",
    "OH": "39",
    "OK": "40",
    "OR": "41",
    "PA": "42",
    "PR": "72",
    "RI": "44",
    "SC": "45",
    "SD": "46",
    "TN": "47",
    "TX": "48",
    "UT": "49",
    "VA": "51",
    "VI": "78",
    "VT": "50",
    "WA": "53",
    "WI": "55",
    "WV": "54",
    "WY": "56",
}


def get_population_demographics_data(filename_to_save):
    with open(filename_to_save, "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(["Name", "FIPS", "Code", "Total Population"])
        for state_name in TRIAL_STATES:
            state_code = STATE_NAMES_TO_CODES[state_name]
            fips = STATE_FIPS[state_code]
            request_url = (
                "https://api.census.gov/data/2018/acs/acs1/cprofile?get=CP05_2018_033E&for=STATE:"
                + fips
                + "&key="
                + API_KEY
            )
            r = requests.get(url=request_url)
            data = r.json()  #  [["CP05_2018_033E", "state"], ["7171646", "04"]]
            population = data[1][0]
            writer.writerow([state_name.capitalize(), fips, state_code, population])


if __name__ == "__main__":
    get_population_demographics_data("states.csv")
