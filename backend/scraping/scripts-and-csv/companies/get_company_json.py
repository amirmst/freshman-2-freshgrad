import requests
import json
import csv
import urllib.parse
import time


my_api_key = "b56375429a70b8466c191c503fda1435"
url = "https://api.uplead.com/v2/company-search?company="
headers = {"Authorization": my_api_key}

with open("company_names.csv") as read_obj, open(
    "companies_model_no_img.csv", "w", newline=""
) as write_obj:
    writer = csv.writer(write_obj)
    csv_reader = csv.reader(read_obj, delimiter=",")
    writer.writerow(
        [
            "Company",
            "Address",
            "Alexa Rank",
            "City",
            "Company Name",
            "Description",
            "Domain",
            "Employees",
            "Exchange",
            "Industry",
            "Linkedin_url",
            "Revenue",
            "Ticker",
            "Year Founded",
        ]
    )
    for row in csv_reader:
        comapny_name = row[0]
        query = urllib.parse.quote(comapny_name)
        response = requests.get(url + query, headers=headers).json()
        print(response)
        time.sleep(1.2)
