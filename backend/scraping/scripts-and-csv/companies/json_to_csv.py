import pandas as pd

company_list = ["amazon", "apple", "dell"]
data_frames = list()
concat = False

for company in company_list:
    json_filepath = "C:\\Users\\ChanDaMan\\Desktop\\SWE\idb1\\freshman-2-freshgrad\\scraping\\scripts-and-csv\\companies\\company_json_files\\"
    json_df = pd.read_json(json_filepath + company + ".json")

    print("ORIGINAL DF ----------------")
    print(json_df)
    # json_df = json_df.iloc[2:, :]
    print(json_df.index.values)

    # clean up the df
    json_df = json_df.T
    # grab the attributes we care about
    attributes = [
        "address",
        "alexa_rank",
        "city",
        "company_name",
        "description",
        "domain",
        "employees",
        "exchange",
        "industry",
        "linkedin_url",
        "revenue",
        "ticker",
        "year_founded",
    ]

    json_df = json_df.filter(items=attributes, axis=1)
    print(json_df)
    if not concat:
        save_path = "C:\\Users\\ChanDaMan\\Desktop\\SWE\\idb1\\freshman-2-freshgrad\\scraping\\scripts-and-csv\\companies\\"
        json_df.to_csv(save_path + company + ".csv", header=True, index=False)
    else:
        data_frames.append(json_df)
if concat:
    save_path = "C:\\Users\\ChanDaMan\\Desktop\\SWE\\idb1\\freshman-2-freshgrad\\scraping\\scripts-and-csv\\companies\\company_concat.csv"
    df = pd.concat(data_frames, axis=0)
    df.to_csv(save_path, header=True, index=False)
