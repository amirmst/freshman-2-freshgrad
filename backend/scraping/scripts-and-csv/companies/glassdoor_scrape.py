import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import pandas as pd
import time
from selenium.common.exceptions import NoSuchElementException


def parse_overview(df, overview_list):
    row = list()
    row.append(overview_list[0].split(" Overview")[0])
    # only grabbing stats, not glassdoor rewards
    descriptors = overview_list[1:8]
    for entry in descriptors:
        row.append(entry.split(" ", 1)[1])
    # This was about info, but it varies in position between companies as some have competitors and others "dont"
    # row.append(overview_list[8])
    return row


def parse_stats(df, stats_list, row_data):
    rating_loc = 1
    friend_loc = 2
    ceo_approval_loc = 4
    ceo_loc = 6
    indices = [rating_loc, friend_loc, ceo_approval_loc, ceo_loc]
    for index in indices:
        row_data.append(stats_list[index])


def parse_interview(df, interview_list, row_data):
    interview_exp = interview_list[1:4]
    interview_ref = interview_list[5:8]
    for exp in interview_exp:
        row_data.append(exp.split(" ")[1])

    for ref in interview_ref:
        row_data.append(ref.split(" ")[-1])
    interview_diff_loc = 11
    row_data.append(interview_list[interview_diff_loc])


def get_img_url(web_driver, url_list):
    # images = web_driver.find_elements_by_tag_name('img')
    # for image in images :
    #     print(image.get_attribute('src'))
    time.sleep(3)
    images = web_driver.find_element_by_css_selector(
        "span.sqLogo.tighten.lgSqLogo.logoOverlay"
    )
    url_list.append(images.get_attribute("innerHTML").split('"')[1])
    print(url_list)
    print("-" * 10)


columns = [
    "Company Name",
    "Website",
    "Headquarters",
    "Size",
    "Year Founded",
    "Company Type",
    "Industry",
    "Revenue",
    "Glassdoor Rating",
    "Recommend to a Friend",
    "CEO Approval Rating",
    "CEO",
    "Positive Interview Experience",
    "Neutral Interview Experience",
    "Negative Interview Experience",
    "Interview from Online Application",
    " Interview from Employee Referral",
    "Interview from Recruiter",
    "Interview Difficulty",
]
# df = pd.DataFrame(columns = columns)
urls = list()

# this is going to be different for other users, this is the path to the exe chromedriver, add your own path accordingly
driver_path = r"C:\Users\chand\Desktop\SWE\freshman-2-freshgrad\scraping\scripts-and-csv\companies\chromedriver.exe"
driver = webdriver.Chrome(driver_path)
# need to let it go full screen otherwise it cant log in :(
driver.maximize_window()
driver.implicitly_wait(3)
url = "https://www.glassdoor.com/Reviews/index.htm"
driver.get(url)

# log in
user = "freshman2freshgrad@gmail.com"
password = "DowningDoorScraping!"

sign_in = driver.find_element_by_class_name("sign-in")
sign_in.click()
user_email = driver.find_element_by_name("username")
user_email.send_keys(user)
user_password = driver.find_element_by_name("password")
user_password.send_keys(password)
submit = driver.find_element_by_name("submit")
submit.click()

# wait for the login to occur
time.sleep(3)
# get geotracking cleared
location = driver.find_element_by_id("LocationSearch")
location.send_keys(Keys.CONTROL + "a")
location.send_keys(Keys.DELETE)

f = open("scraping\scripts-and-csv\companies\company_names.txt", "r")
companies = f.readlines()
failures = list()
# companies = ['Berkshire Hathaway', 'Apple', 'Google', 'BillyJoelSoul', 'Phillips 66', 'Cisco']

for company_name in companies:
    company_found = True
    # search for company
    company_name = company_name.split("\n")[0]
    try:
        search = driver.find_element_by_class_name("keyword")
    except NoSuchElementException:
        try:
            search = driver.find_element_by_id("sc.keyword")
        except NoSuchElementException:
            print("didnt find that either...")
    search.send_keys(Keys.CONTROL + "a")
    search.send_keys(Keys.DELETE)
    search.send_keys(company_name)
    search.send_keys(Keys.ENTER)

    time.sleep(3)

    """
    Three cases on glassdoor that I've seen so far:
    1) Company name searched matches with name on glassdoor --Apple--
    2) Company name does not match directly so we need to pick one of the options --Cisco--
    3) it immediately pulls up the company info --Phillips 66--
    """
    try:
        try:
            # Case 1 & 2 now just picking the first search result
            possible_company = driver.find_element_by_class_name(
                "single-company-result"
            )
            text_companies = list()
            line = possible_company.text.split("\n")
            text_companies.append(line)

            if len(text_companies) != 0:
                # Case 3
                company = driver.find_element_by_link_text(text_companies[0][0]).click()
                time.sleep(3)
            else:
                company_found = False

        except NoSuchElementException:
            # Case 3
            pass

        try:
            overview = driver.find_element_by_id("EmpBasicInfo").text
        except NoSuchElementException:
            print("Company - " + company_name + " - not found")
            failures.append(company_name)
            company_found = False

        if company_found:
            """
            comment this back in for normal behavior
            overview_list = overview.split('\n')
            print(overview)

            stats = driver.find_element_by_id('EmpStats').text
            print("-" * 10)
            stats_list = stats.split('\n')
            print(stats_list)

            interview = driver.find_element_by_class_name('interviewStatsBody').text
            print("-" * 10)
            interview_list = interview.split('\n')
            print(interview_list)

            print(df)
            row_data = parse_overview(df, overview_list)
            parse_stats(df, stats_list, row_data)
            parse_interview(df, interview_list, row_data)
            get_img_url()
            # print(row_data)
            # df.loc[len(df)] = row_data
            print(df)
            """
            get_img_url(driver, urls)
    except:
        print("something went wrong, not adding -- " + company_name)
        failures.append(company_name)

print("These companies did not get added for various reasons: " + str(failures))


# for images---uncomment for normal use
df = pd.DataFrame(urls, columns=["ImgURL"])

path = "scraping\scripts-and-csv\companies\company_csv_files\company_logos.csv"
df.to_csv(path)
