import pandas as pd


# read in csv
path = r"scraping\scripts-and-csv\companies\company_csv_files\all_company_info.csv"
df = pd.read_csv(path)

hq_col = df.iloc[:, 2]
print(hq_col)

city_list = list()
state_list = list()

for row in hq_col:
    row_data = row.split(", ")
    city_list.append(row_data[0])
    state_list.append(row_data[1])

print(city_list)
print(state_list)

df["City"] = city_list
df["State"] = state_list

save_path = (
    r"scraping\scripts-and-csv\companies\company_csv_files\all_company_info_fixed.csv"
)
df.to_csv(save_path)
