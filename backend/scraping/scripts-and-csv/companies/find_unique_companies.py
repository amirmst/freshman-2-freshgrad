import requests
import json
import csv
import urllib.parse
import time

companies1 = set()
companies2 = set()

with open("companies_list.csv") as read_obj:
    csv_reader = csv.reader(read_obj, delimiter=",")
    for row in csv_reader:
        companies1 |= {row[0]}

with open("forbes_list.csv") as read_obj:
    csv_reader = csv.reader(read_obj, delimiter=",")
    for row in csv_reader:
        companies2 |= {row[0]}

with open("company_names.csv", "w", newline="") as write_obj:
    csv_writer = csv.writer(write_obj)
    non_duplicate_companies = companies1.union(companies2)
    for company in non_duplicate_companies:
        print(company)
