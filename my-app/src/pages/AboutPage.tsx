import React from 'react';

export class AboutPage extends React.Component {
    render() {
        return (
            <div>
  <meta charSet="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Freshman to Fresh Grad</title>
  {/* Bootstrap core CSS */}
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  {/* Custom styles for this template */}
  <link href="css/modern-business.css" rel="stylesheet" />
  {/* Page Content */}
  <div className="container">
    {/* Page Heading/Breadcrumbs */}
    <h1 className="mt-4 mb-3">About Freshman to Fresh Grad
    </h1>
    {/* Intro Content */}
    <div className="row">
      <div className="col-lg-6">
        <p>Whether you're an incoming Freshman or an outgoing Fresh Grad, you have some important decisions to make that will shape the rest of your career. If you're anything like us, you've spent countless hours researching what to study, where to study, where to work, and what to work on.</p>
        <p>We set out to create a centralized hub where people could access the information they needed in a few simple clicks. We believe that information is power, so we wish to give users a picture of the path to becoming a software engineer. We hope to eliminate the headache of searching and cross-referencing of information for those looking to dive into software.</p>
      </div>
    </div>
    {/* /.row */}
    {/* Team Members */}
    <h2>Our Team</h2>
    <div className="row">
      <div className="col-lg-4 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/amir_pic.jpg" alt="" />
          <div className="card-body">
            <h4 className="card-title">Amir Mostafavi</h4>
            <h6 className="card-subtitle mb-2 text-muted">Back-end: API</h6>
            <p id="Amir Mostafavi" className="card-text">Unit Tests: 0</p>
          </div>
        </div>
      </div>
      <div className="col-lg-4 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/connor_pic.jpg" alt="" />
          <div className="card-body">
            <h4 className="card-title">Connor Hubbard</h4>
            <h6 className="card-subtitle mb-2 text-muted">Front-end</h6>
            <p id="Connor Hubbard" className="card-text">Unit Tests: 0</p>
          </div>
        </div>
      </div>
      <div className="col-lg-4 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/chandler_pic.jpg" alt="" />
          <div className="card-body">
            <h4 className="card-title">Chandler Ochs</h4>
            <h6 className="card-subtitle mb-2 text-muted">Back-end: DB</h6>
            <p id="Chandler Ochs" className="card-text">Unit Tests: 0</p>
          </div>
        </div>
      </div>
    </div>
    {/* another row*/}
    <div className="row">
      <div className="col-lg-4 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/dillan_pic.jpg" alt="" />
          <div className="card-body">
            <h4 className="card-title">Dillan Holland</h4>
            <h6 className="card-subtitle mb-2 text-muted">Back-end: DB</h6>
            <p id="Dillan Holland" className="card-text">Unit Tests: 0</p>
          </div>
        </div>
      </div>
      <div className="col-lg-4 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/zac_pic.png" alt="" />
          <div className="card-body">
            <h4 className="card-title">Zachary May</h4>
            <h6 className="card-subtitle mb-2 text-muted">Back-end: API</h6>
            <p id="Zachary May" className="card-text">Unit Tests: 0</p>
          </div>
        </div>
      </div>
      <div className="col-lg-4 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/sooyong_pic.JPG" alt="" />
          <div className="card-body">
            <h4 className="card-title">Sooyong Lee</h4>
            <h6 className="card-subtitle mb-2 text-muted">Frontend</h6>
            <p id="Sooyong Lee" className="card-text">Unit Tests: 0</p>
          </div>
        </div>
      </div>
    </div>
    <h2>Project Statistics</h2>
    <li>Unit Tests: 0</li>
    <p id="total_commit" />
    <p id="total_issue" />
    <h2>Tools</h2>
    {/* /.row */}
    <div className="row">
      <div className="col-lg-3 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/aws.png" alt="" />
          <div className="card-body">
            <h4 className="card-title">
              <a href="https://aws.amazon.com/">Amazon Web Services</a>
            </h4>
            <p className="card-text">Server for hosting</p>
          </div>
        </div>
      </div>
      <div className="col-lg-3 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/postman.jpg" alt="" />
          <div className="card-body">
            <h4 className="card-title">
              <a href="https://documenter.getpostman.com/view/10532101/SzKYQcpW?version=latest">Our Postman</a>
            </h4>
            <p className="card-text">API design and documentation</p>
          </div>
        </div>
      </div>
      <div className="col-lg-3 mb-4">
        <div className="card h-100 text-center">
          <img className="card-img-top" src="./images/gitlab.png" alt="" />
          <div className="card-body">
            <h4 className="card-title">
              <a href="https://gitlab.com/amirmst/freshman-2-freshgrad">Our Gitlab</a>
            </h4>
            <p className="card-text">Contains source code</p>
          </div>
        </div>
      </div>
    </div>
    <h2>Data Sources</h2>
    <ul>
      <a href="https://www.uplead.com/">UpLead<br /></a>
      <a href="https://jobs.github.com/api">Git Jobs<br /></a>
      <a href="https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates">ArcGis<br /></a>
      <a href="https://api.data.gov/ed/collegescorecard/v1/schools">US Government College Info API<br /></a>
      <a href="https://www.numbeo.com/api/cities">Numbeo<br /></a>
      <a href="https://www.numbeo.com/api/indicies">Numbeo City Info<br /></a>
    </ul>
  </div>
  {/* /.container */}
  {/* Bootstrap core JavaScript */}
</div>
        )
    }
    componentDidMount() {
        const script = document.createElement("script");
        script.src = "./js/about.js";
        script.async = true;
        document.body.appendChild(script);
    }
}
export default AboutPage;