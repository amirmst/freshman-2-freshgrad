import React from 'react';

export class UniversityInstancePage extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }
    state = {
        info: []
    }
    componentDidMount() {
        fetch('http://api.freshman2freshgrad.com/universities?name='+this.props.match.params.university)
        .then(res => res.json())
        .then((data) => {
          this.setState({info: data})
          console.log(this.state.info)
        })   
        .catch(console.log)
    }
    render() {
        return (
<div className="container">
  {/* Page Heading/Breadcrumbs */}
  <h1 className="mt-4 mb-3">{this.state.info["university" as any]}
  </h1>
  {/* Portfolio Item Row */}
  <div className="row">
    <div className="col-md-8">
      <img className="img-fluid" src={this.state.info["image_url" as any]} alt="" />
    </div>
    <div className="col-md-4">
      <h3 className="my-3">Attributes</h3>
      <ul>
        <li>Latitude/Longitude: {this.state.info["latitude" as any]}, {this.state.info["longitude" as any]}</li>
        <li>City: {this.state.info["city" as any]}</li>
        <li>CS Ranking: {this.state.info["cs_ranking" as any]}</li>
        <li>Undergrad Population: {this.state.info["undergrad_population" as any]}</li>
        <li>Gender Demographics:</li>
        <ul>
          <li>{this.state.info["male_percentage" as any]}% male</li>
          <li>{this.state.info["female_percentage" as any]}% female</li>
        </ul>
        <li>Racial Demographics:</li>
        <ul>
          <li>{this.state.info["white_percentage" as any]}% white</li>
          <li>{this.state.info["black_percentage" as any]}% black</li>
          <li>{this.state.info["asian_percentage" as any]}% asian</li>
          <li>{this.state.info["hispanic_percentage" as any]}% hispanic</li> 
        </ul>
        <li>Tuition:</li>
        <ul>
          <li>In-State: {this.state.info["in_state_tuition" as any]}</li>
          <li>Out of State: {this.state.info["out_of_state_tuition" as any]}</li>  
        </ul>
        <li>Graduation Rate: {this.state.info["graduation_rate" as any]}</li>
        <li>Admission Rate: {this.state.info["admission_rate" as any]}%</li>
        <li>Median Test Scores for Admitted Students:</li>
        <ul>
          <li>SAT Median Reading: {this.state.info["sat_median_reading" as any]}</li>
          <li>SAT Median Math: {this.state.info["sat_median_math" as any]}</li>
          <li>ACT Median English: {this.state.info["act_median_english" as any]}</li>
          <li>ACT Median Math: {this.state.info["act_median_math" as any]}</li>
        </ul>
      </ul>
    </div>
  </div>
</div>
        )
    }
}
export default UniversityInstancePage;