import React from 'react';

export class CompaniesPage extends React.Component {
    render() {
        return (
            <div>
  <meta charSet="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Freshman to Fresh Grad</title>
  {/* Page Content */}
  <div className="container">
    {/* Page Heading/Breadcrumbs */}
    <h1 className="mt-4 mb-3">Companies
    </h1>
    <h4>Where your path will take you.</h4>
    <div className="row">
      <div className="col-lg-3 col-md-4 col-sm-6 portfolio-item">
        <div className="card h-100">
          <a href="apple.html"><img className="card-img-top" src="./images/apple.jpg" alt="" /></a>
          <div className="card-body">
            <h4 className="card-title">
              <a href="apple.html">Apple</a>
            </h4>
            <p className="card-text">Cupertino, CA<br />Alexa_rank: 59<br />Domain: apple.com<br />Employees: Over 10,000<br />Year Founded: 1976</p>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-4 col-sm-6 portfolio-item">
        <div className="card h-100">
          <a href="amazon.html"><img className="card-img-top" src="./images/amazon.jpg" alt="" /></a>
          <div className="card-body">
            <h4 className="card-title">
              <a href="amazon.html">Amazon</a>
            </h4>
            <p className="card-text">Seattle, WA<br />Alexa_rank: 14<br />Domain: amazon.com<br />Employees: Over 10,000<br />Year Founded: 1994</p>
          </div>
        </div>
      </div>
      <div className="col-lg-3 col-md-4 col-sm-6 portfolio-item">
        <div className="card h-100">
          <a href="dell.html"><img className="card-img-top" src="./images/dell.jpg" alt="" /></a>
          <div className="card-body">
            <h4 className="card-title">
              <a href="dell.html">Dell</a>
            </h4>
            <p className="card-text">Austin, TX<br />Alexa_rank: 398<br />Domain: dell.com<br />Employees: Over 10,000<br />Year Founded: 1996</p>
          </div>
        </div>
      </div>
    </div>
    {/* Pagination */}
    <ul className="pagination justify-content-center">
      <li className="page-item">
        <a className="page-link" href="/#" aria-label="Previous">
          <span aria-hidden="true">«</span>
          <span className="sr-only">Previous</span>
        </a>
      </li>
      <li className="page-item">
        <a className="page-link" href="/#">1</a>
      </li>
      <li className="page-item">
        <a className="page-link" href="/#">2</a>
      </li>
      <li className="page-item">
        <a className="page-link" href="/#">3</a>
      </li>
      <li className="page-item">
        <a className="page-link" href="/#" aria-label="Next">
          <span aria-hidden="true">»</span>
          <span className="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </div>
  {/* /.container */}
  {/* Bootstrap core JavaScript */}
</div>

        )
    }
}
export default CompaniesPage;