import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import CitiesPage from './pages/CitiesPage';
import UniversitiesPage from './pages/UniversitiesPage';
import CompaniesPage from './pages/CompaniesPage';
import UniversityInstancePage from './pages/UniversityInstancePage';
import NavBar from './components/NavBar';

function App() {
  return (
    <div>
      <Router>
        <NavBar/>
        <Switch>
          <Route exact  path='/' component={HomePage}/>
          <Route path='/about' component={AboutPage}/>
          <Route exact path='/cities' component={CitiesPage}/>
          <Route exact path='/universities' component={UniversitiesPage}/>
          <Route path='/universities/:university' component={UniversityInstancePage}/>
          <Route exact path='/companies' component={CompaniesPage}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
