function getInfo() {
        
    let usernames = ["amirmst", "chandlerochs", "hubchub", "dillan0321", "sooyonglee", "zacharymay"];
    let names = ["Amir Mostafavi", "Chandler Ochs", "Connor Hubbard", "Dillan Holland", "Sooyong Lee", "Zachary May"];
    var total_commit = 0;
    var total_issue = 0;

    //bug was here
    for(let i = 0; i <= names.length; i++) {
        fetch("https://gitlab.com/api/v4/projects/17022161/repository/contributors")
        .then((response) => response.json()).then((myJson)=> {
            curName = myJson[i].name;
            numCommits = myJson[i].commits;
            var commitCount = document.createElement('p');
            commitCount.innerHTML = "Commits: " + numCommits;
            if(curName != 'amirmst')
              document.getElementById(curName).appendChild(commitCount);
              total_commit += numCommits;
              if (i==names.length) {
                  var num_commits = document.createElement("li");
                  num_commits.innerHTML = total_commit + " commits";
                  document.getElementById("total_commit").appendChild(num_commits);
              }
        })
    }

    for(let i = 0; i < names.length; i++) {
        fetch("https://gitlab.com/api/v4/projects/17022161/issues_statistics?author_username=" + usernames[i])
        .then((response) => response.json()).then((myJson)=> {
            numIssues = myJson.statistics.counts.all;
            var issueCount = document.createElement('p');
            issueCount.innerHTML = "Issues: " + numIssues;
            document.getElementById(names[i]).appendChild(issueCount);
            total_issue += numIssues;
            if(i==names.length-1) {
                var num_issues = document.createElement("li");
                num_issues.innerHTML = total_issue + " issues";
                document.getElementById("total_issue").appendChild(num_issues);
            }
        })
    }
}
getInfo();