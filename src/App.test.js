import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import App from './App';
import Enzyme, { mount, shallow, configure } from 'enzyme';
import NavBar from './components/NavBar';
import Adapter from 'enzyme-adapter-react-16';
import CitiesPage from './pages/CitiesPage';
import UniversitiesPage from './pages/CitiesPage';
import CompaniesPage from './pages/CompaniesPage';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';

configure({ adapter: new Adapter() });

describe('App', function() {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('renders the navbar', () => {
    const div = document.createElement('div');
    ReactDOM.render(<NavBar />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('app contents-1', () => {
    const app = mount(<App />);
    expect(app.find('Router').length).toEqual(1);
  });
  it('app contents-2', () => {
    const app = mount(<App />);
    expect(app.find('Route').length).toEqual(1);
  });
  it('app contents-3', () => {
    const app = mount(<App />);
    expect(app.find('NavBar').length).toEqual(1);
  });

  it('renders the CitiesPage', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CitiesPage />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('renders the UniversitiesPage', () => {
    const div = document.createElement('div');
    ReactDOM.render(<UniversitiesPage />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('renders the CompaniesPage', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CompaniesPage />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('renders the HomePage', () => {
    const div = document.createElement('div');
    ReactDOM.render(<HomePage />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
  it('renders the AboutPage', () => {
    const div = document.createElement('div');
    ReactDOM.render(<AboutPage />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

