import React from 'react';
import * as d3 from 'd3';

class StateRestaurantBubbleChart extends React.Component {
    constructor(props: any) {
        super(props);
        this.createBubbleChart = this.createBubbleChart.bind(this);
    }
    componentDidMount() {
        this.createBubbleChart();
    }
    createBubbleChart() {
        var json = {
            'children': [
              {'name': 'New York', 'value': 40},
              {'name': 'California', 'value': 360},
              {'name': 'Texas', 'value': 260},
              {'name': 'Illinois', 'value': 20},
              {'name': 'Pennsylvania', 'value': 40},
              {'name': 'Arizona', 'value': 140},
              {'name': 'Kansas', 'value': 20},
              {'name': 'Indiana', 'value': 40},
              {'name': 'Florida', 'value': 100},
              {'name': 'Ohio', 'value': 80},
              {'name': 'North Carolina', 'value': 100},
              {'name': 'Michigan', 'value': 20},
              {'name': 'Tennessee', 'value': 40},
              {'name': 'Washington', 'value': 20},
              {'name': 'Colorado', 'value': 60},
              {'name': 'District Of Columbia', 'value': 20},
              {'name': 'Massachusetts', 'value': 20},
              {'name': 'Missouri', 'value': 40},
              {'name': 'Maryland', 'value': 20},
              {'name': 'Oklahoma', 'value': 40},
              {'name': 'Kentucky', 'value': 40},
              {'name': 'Oregon', 'value': 20},
              {'name': 'Nevada', 'value': 80},
              {'name': 'Wisconsin', 'value': 40},
              {'name': 'New Mexico', 'value': 20},
              {'name': 'Virginia', 'value': 80},
              {'name': 'Georgia', 'value': 20},
              {'name': 'Nebraska', 'value': 40},
              {'name': 'Minnesota', 'value': 40},
              {'name': 'Louisiana', 'value': 40},
              {'name': 'Hawaii', 'value': 20},
              {'name': 'Alaska', 'value': 20},
              {'name': 'New Jersey', 'value': 40},
              {'name': 'Idaho', 'value': 20}
            ]
          }
          
          var diameter = 600,
              color = d3.scaleOrdinal(d3.schemeCategory10);
          
          var bubble = d3.pack()
            .size([diameter, diameter])
            .padding(5);
          
          var margin = {
            left: 0,
            right: 100,
            top: 0,
            bottom: 0
          }
          
          var svg = d3.select('#stateRestaurantBubbleChart').append('svg')
            .attr('viewBox','0 0 ' + (diameter + margin.right) + ' ' + diameter)
            .attr('width', (diameter + margin.right))
            .attr('height', diameter)
            .attr('class', 'chart-svg');
          
          var root:any = d3.hierarchy(json)
            .sum(function(d:any) { return d.value; })
            .sort(function(a:any, b:any) { return b.value - a.value; });
          
          bubble(root);
          
          var node = svg.selectAll('.node')
            .data(root.children)
            .enter()
            .append('g').attr('class', 'node')
            .attr('transform', function(d:any) { return 'translate(' + d.x + ' ' + d.y + ')'; })
            .append('g').attr('class', 'graph');
          
          node.append("circle")
            .attr("r", function(d:any) { return d.r; })
            .style("fill", function(d:any) { 
              return color(d.data.name); 
            });
          
          node.append("text")
            .attr("dy", ".3em")
            .style("text-anchor", "middle")
            .text(function(d:any) { return d.data.name; })
            .style("fill", "#000000");
          
          svg.append("g")
            .attr("class", "legendOrdinal")
            .attr("transform", "translate(600,40)"); 
    }
    render() {
        return <div id="stateRestaurantBubbleChart"></div>
    }
}
export default StateRestaurantBubbleChart;