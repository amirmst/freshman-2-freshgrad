import requests
import json

state_restaurant = {}

page_num = 1
response = requests.get('https://api.collegefitfor.me/restaurants?page={}'.format(page_num))

while response.status_code == 200:
    response = response.json()
    for rest in response:
        state = rest.get('state_name', 'N/A')
        if state_restaurant.get(state, None) == None:
            state_restaurant[state] = 0
            
        state_restaurant[state] = state_restaurant[state] + 1

    page_num += 1
    response = requests.get('https://api.collegefitfor.me/restaurants?page={}'.format(page_num))

with open('uniRestaurant.json', 'w') as fp:
    json.dump(state_restaurant, fp)
