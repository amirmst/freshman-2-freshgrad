import requests




def get_all_for_price(price_code):
    res = 0
    page_num = 1
    response = requests.get('https://api.collegefitfor.me/restaurants/filter/?price={}&page={}'.format(price_code, page_num))
    while response.status_code == 200:
        response = response.json()
        res += len(response)
        page_num += 1
        response = requests.get('https://api.collegefitfor.me/restaurants/filter/?price={}&page={}'.format(price_code, page_num))
    return res 

def get_perc(part, tot):
    return (part / tot) * 100

one_dollars = get_all_for_price('$')
two_dollars = get_all_for_price('$$')
three_dollars = get_all_for_price('$$$')
four_dollars = get_all_for_price('$$$$')

total = one_dollars + two_dollars + three_dollars + four_dollars

print("TOTAL: {}".format(total))
print('$: {}, $$: {}, $$$: {}, $$$$: {}'.format(get_perc(one_dollars, total),get_perc(two_dollars, total),get_perc(three_dollars, total),get_perc(four_dollars, total)))

