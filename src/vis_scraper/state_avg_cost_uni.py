import requests


state_count = {}
state_cost = {}

page_num = 1
response = requests.get('https://api.collegefitfor.me/universities?page={}'.format(page_num))

while response.status_code == 200:
    response = response.json()
    for uni in response:
        state = uni.get('state_name', 'N/A')
        cost = int(uni.get('cost_per_year', 0))
        if cost != 0:
            if state_count.get(state, None) == None:
                state_count[state] = 0
            if state_cost.get(state, None) == None:
                state_cost[state] = 0
                
            state_count[state] = state_count[state] + 1
            state_cost[state] += cost
    page_num += 1
    response = requests.get('https://api.collegefitfor.me/universities?page={}'.format(page_num))

print('[')
for state in state_count:
    print("{{\n\t\"state\": \"{}\",\n\t\"avg_annual_cost\": {}\n}},".format(state, state_cost[state]/state_count[state]))
print(']')